const gameContainer = document.getElementById("game");
let bestScore = window.localStorage.getItem('bestScore');
if(bestScore === null) {
  document.querySelector('#best').style.display = 'none';
} else {
  document.querySelector('#best').style.display = 'block';
  document.querySelector('#best span').innerHTML = bestScore;
}

const COLORS = [
  "red",
  "blue",
  "green",
  "orange",
  "purple",
  "red",
  "blue",
  "green",
  "orange",
  "purple"
];

const previous = {
  click: '',
  color: ''
};

// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array) {
  let counter = array.length;

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter);

    // Decrease counter by 1
    counter--;

    // And swap the last element with it
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}

let shuffledColors = shuffle(COLORS);

// this function loops over the array of colors
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card

function createDivForFlip(color) {
  const mainDiv = document.createElement("div");
  const flipBox = document.createElement("div");
  const flipFront = document.createElement("div");
  const flipBack = document.createElement("div");

  mainDiv.classList.add('mainDiv');
  flipBox.classList.add('flip-box');
  flipFront.classList.add('flip-front');
  flipBack.classList.add('flip-back',color);

  flipFront.addEventListener('click', handleCardClick);
  
  flipFront.style.backgroundColor = '#808080';
  flipBack.style.backgroundColor = color;

  flipBox.append(flipFront,flipBack);
  mainDiv.append(flipBox);
  
  return mainDiv;
}

function createDivsForColors(colorArray) {
  for (let color of colorArray) {
    // create a new div
    // const newDiv = document.createElement("div");

    // give it a class attribute for the value we are looping over
    // newDiv.classList.add(color);

    const newDiv = createDivForFlip(color);

    // call a function handleCardClick when a div is clicked on
    // newDiv.addEventListener("click", handleCardClick);
    // newDiv.removeEventListener("click", handleCardClick);

    // append the div to the element with an id of game
    gameContainer.append(newDiv);
  }
}

let count = 0;
let scoreCount = 0;

// TODO: Implement this function!
function handleCardClick(event) {
  // you can use event.target to see which element was clicked
  console.log("you clicked",event.target.nextElementSibling.classList.value.split(' ')[1]);
  // event.target.style.backgroundColor = event.target.classList.value;

  event.target.parentElement.classList.add('flip-click');
  
  const currentColor = event.target.nextElementSibling.classList.value.split(' ')[1];

  if(previous.color === '') {
    previous.color = currentColor;
    previous.click = event.target;
    document.querySelector('#game').style.pointerEvents = 'none';
    scoreCount++;
    setTimeout(()=>{
      document.querySelector('#game').style.pointerEvents = 'auto';
    },200);
  } else if (previous.color === currentColor) {
    previous.color = '';
    previous.click = '';
    count++;
    scoreCount++;
  } else {
    document.querySelector('#game').style.pointerEvents = 'none';
    scoreCount++;
    setTimeout(()=>{
      event.target.parentElement.classList.remove('flip-click');
      previous.click.parentElement.classList.remove('flip-click');
      previous.color = '';
      document.querySelector('#game').style.pointerEvents = 'auto';
    },1000);
  }

  document.querySelector('#score').innerHTML = scoreCount;

  if(count === 5){
    if(bestScore == null) {
      window.localStorage.setItem('bestScore', scoreCount);
    } else if (bestScore > scoreCount) {
      window.localStorage.setItem('bestScore', scoreCount);
    }
    bestScore = window.localStorage.getItem('bestScore');

    document.querySelector('#yourScore').innerHTML = scoreCount;
    document.querySelector('#bestScore').innerHTML = bestScore;
    document.querySelector('.won').style.display = 'flex';
  }
}

// when the DOM loads
createDivsForColors(shuffledColors);
